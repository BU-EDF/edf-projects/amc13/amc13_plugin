PACKAGES = \
	amc13 \
	tools 

UHAL_VER_MAJOR ?= 2
UHAL_VER_MINOR ?= 8

$(info $(MAKEFLAGS))

ifeq (${UHAL_VER_MINOR}, 8)
PACKAGES += python
else
$(info "PYTHON BINDINGS NO LONGER SUPPORTED FOR UHAL 2.7")
$(info "IF YOU NEED THESE PLEASE UPGRADE TO UHAL 2.8")
endif



VIRTUAL_PACKAGES = $(addsuffix /.virtual.Makefile,${PACKAGES})

FLAGS = $(ifeq $(MAKEFLAGS) "","",-$(MAKEFLAGS))

TARGETS=clean cleanrpm rpm build all

.PHONY: $(TARGETS)
default: build

$(TARGETS): ${VIRTUAL_PACKAGES}

${VIRTUAL_PACKAGES}:
	${MAKE} ${FLAGS} -C $(@D) $(MAKECMDGOALS)


TEST:
	echo ${FLAGS}

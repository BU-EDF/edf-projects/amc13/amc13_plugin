/*
 *
 * File: amc13/python/exceptions.hpp
 * Author: Tom Williams
 * Date: March 2015
 *   (Largely copy of corresponding file in uHAL Python bindings)
 */

#ifndef AMC13_PYTHON_EXCEPTIONS_HPP
#define AMC13_PYTHON_EXCEPTIONS_HPP


#include "boost/python/exception_translator.hpp"


namespace pybind11 {
class module_;
}

namespace amc13py{

  //! Wraps all amc13 exceptions (i.e. creates corresponding Python classes, and regsiters appropriate exception translators)
  void wrap_exceptions(pybind11::module_&);

}


#endif


#include "amc13/python/exceptions.hpp"


#include "pybind11/pybind11.h"

#include "amc13/Exception.hh"


namespace py = pybind11;


void amc13py::wrap_exceptions(pybind11::module_& aModule)
{
  // Base amc13 exception (fallback for derived exceptions if not wrapped)
  auto baseException = py::register_exception<amc13::Exception::exBase>(aModule, "exBase", PyExc_Exception);

  // Derived amc13 exceptions
  py::register_exception<amc13::Exception::BadChip>(aModule, "BadChip", baseException);
  py::register_exception<amc13::Exception::NULLPointer>(aModule, "NULLPointer", baseException);
  py::register_exception<amc13::Exception::UnexpectedRange>(aModule, "UnexpectedRange", baseException);
  py::register_exception<amc13::Exception::BadFileFormat>(aModule, "BadFileFormat", baseException);
  py::register_exception<amc13::Exception::BadAMC13>(aModule, "BadAMC13", baseException);
  py::register_exception<amc13::Exception::BadValue>(aModule, "BadValue", baseException);
  py::register_exception<amc13::Exception::BadFile>(aModule, "BadFile", baseException);
}

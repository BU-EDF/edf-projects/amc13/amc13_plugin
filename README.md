# amc13_plugin

amc13 plugin for BUTool


Not for you!  Do not use this! -Dan
=======
 
AMC13 software
==============

This repo contains the AMC13 C++ library for connecting to AMC13 Modules and a debugging command-line tool called AMC13Tool2.

When cloning this repo, to download all submodules, use the syntax "git clone --recurse-submodules <REPOSITORY>"

To build all of these just type "make" in the base directory. 

The default version of uHAL supported for this repository is 2.8.  If you are using 2.7, you must instead type "make UHAL_VER_MINOR=7".  Be aware that this will skip compilation for the python bindings, as they are no longer supported for uHAL 2.7.

Questions/bugs: email dgastler@bu.edu

Directories:

./cfg
	This directory contains connection files, address tables, and address table tools. 

./amc13
	This is the source code for the AMC13 class, which gives access to the AMC13 hardware.

./tools
	This is the source code for AMC13Tool, which is a command line tool for interacting with an AMC13.

./tools/bin
	AMCTool binary location

./amc13/lib 
      AMC13 library location

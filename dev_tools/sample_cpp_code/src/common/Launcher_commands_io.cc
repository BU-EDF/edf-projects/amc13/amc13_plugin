#include "Launcher.hh"

//
// read one or more registers
// one argument may be: address, single name or regular expression
// second argument is count to read from each address
// last argument may be "D" for doublewords (64-bits)
//
int Launcher::Read( std::vector<std::string> strArg,std::vector<uint64_t> intArg) {

  std::vector<std::string> nodes;
  uhal::HwInterface* hw = defaultModule()->hw;
    
  bool numericAddr = true;
  uint32_t addr = 0;
  uint32_t val;
  uint64_t vald;
  int count = 1;
    
  std::string flags("");
    
  // sort out arguments
  switch( strArg.size()) {
  case 0:			// no arguments is an error
    printf("Need at least an address\n");
    return 0;
  case 3:			// third is flags
    flags = strArg[2];
    // fall through to others
  case 2:			// second is either count or flags
    if( isdigit( strArg[1].c_str()[0]))
      count = intArg[1];
    else
      flags = strArg[1];
    // fall through to address
  case 1:			// one must be an address
    numericAddr = isdigit( strArg[0].c_str()[0]);
    addr = uint32_t( intArg[0]);
    //    printf("Examined %s and determined it is %s\n",
    //	   strArg[0].c_str(), numericAddr ? "numeric" : "non-numeric");
    break;
  default:
    printf("Too many arguments after command\n");
    return 0;
  }
   
  std::transform( flags.begin(), flags.end(), flags.begin(), ::toupper);

  bool b64 = (flags.find("D") != std::string::npos);
  bool nzer = (flags.find("N") != std::string::npos);

  uhal::ValWord<uint32_t> vw; //valword for transaction

  if( !numericAddr) {		// reg name, first make uppercase
    std::string saddr = strArg[0];
    //    std::transform( saddr.begin(), saddr.end(), saddr.begin(), ::toupper);
    // try to look up reg name, complain if not found
    printf("Looking for \"%s\"\n", saddr.c_str());
    try {
      addr = hw->getNode( saddr).getAddress();
      printf("Name %s translated to address 0x%x\n", saddr.c_str(), addr);
    } catch( const uhal::exception::NoBranchFoundWithGivenUID ) {
      printf("Register not found: %s\n", saddr.c_str());
    }
  }
      
  // count specified?
  // if so, read whole words only
  if( count > 1) {

    // print multi-value, 32- or 64-bit
    int wpl = b64 ? 4 : 8;
    int inc = b64 ? 2 : 1;
      
    for( int i=0; i<count; i++) {
      vw = hw->getClient().read(  addr+i*inc); // start the transaction
      hw->getClient().dispatch(); // force the transaction
      vald = vw.value();
      if( b64) {
	vw = hw->getClient().read(  addr+i*inc+1);	
	hw->getClient().dispatch();
	vald = vald | ( (uint64_t)vw.value() << 32);
      }
      if( i % wpl == 0)
	printf("%08x: ",  addr+i*inc);
      printf(" %0*llx", b64?16:8, (unsigned long long)vald);
      if( i % wpl == (wpl-1))
	printf("\n");
    }
    if( count % wpl != (wpl-1))
      printf("\n");
      
  } else {
    // count not specified

    if( numericAddr) {
      vw = hw->getClient().read(addr);
      hw->getClient().dispatch();
      val = vw.value();
      printf("%08x: %08x\n", addr, val);
    } else {
      nodes = myMatchNodes( hw, strArg[0]);
      if( nodes.size()) {
	for( size_t i=0; i<nodes.size(); i++) {
	  vw = hw->getClient().read( hw->getNode(nodes[i]).getAddress() );
	  hw->getClient().dispatch();
	  val = vw.value();
	  //	  val = defaultAMC13()->read(chip,nodes[i]);
	  if( !nzer || (val != 0))
	    printf("%50s: 0x%08x\n", nodes[i].c_str(), val);
	}
      }

    }
  }

    
  return 0;
}
  

// it's wacko that the std library doesn't have this!
void ReplaceStringInPlace(std::string& subject, const std::string& search,
			  const std::string& replace) {
  size_t pos = 0;
  while ((pos = subject.find(search, pos)) != std::string::npos) {
    subject.replace(pos, search.length(), replace);
    pos += replace.length();
  }
}


//
// return a list of nodes matching regular expression
// convert regex so "." is literal, "*" matches any string
// "perl:" prefix leaves regex unchanged
//
std::vector<std::string> myMatchNodes( uhal::HwInterface* hw, const std::string regex)
{
  std::string rx = regex;
    
  //  std::transform( rx.begin(), rx.end(), rx.begin(), ::toupper);
    
  if( rx.size() > 6 && rx.substr(0,5) == "PERL:") {
    printf("Using PERL-style regex unchanged\n");
    rx = rx.substr( 5);
  } else {
    ReplaceStringInPlace( rx, ".", "#");
    ReplaceStringInPlace( rx, "*",".*");
    ReplaceStringInPlace( rx, "#","\\.");
  }
    
  return hw->getNodes( rx);
}
  
  


  
int Launcher::ListNodes(std::vector<std::string> strArg,
			std::vector<uint64_t> intArg) {
  std::vector<std::string> nodes;
  std::string rx;
  uhal::HwInterface * brd = NULL;    
  bool debug = false;
  bool describe = false;
  bool help = false;

  if( strArg.size() < 1) {
    printf("Need regular expression after command\n");
    return 0;
  }
    
  if( strArg.size() > 1) {
    switch( toupper(strArg[1].c_str()[0])) {
    case 'D':
      debug = true;
      break;
    case 'V':
      describe = true;
      break;
    case 'H':
      help = true;
      break;
    }
  }

  brd = defaultModule()->hw;	// no T1 or T23 here

  nodes = myMatchNodes( brd, strArg[0]);
    
  int n = nodes.size();
  printf("%d nodes matched\n", n);
  if( n)
    for( int i=0; i<n; i++) {
      // get various node attributes to display
      const uhal::Node& node = brd->getNode( nodes[i]);
      uint32_t addr = node.getAddress();
      uint32_t mask = node.getMask();
      uint32_t size = node.getSize();
      uhal::defs::BlockReadWriteMode mode = node.getMode();
      uhal::defs::NodePermission perm = node.getPermission();
      std::string s = "";
      const std::string descr = node.getDescription();
      switch( mode) {
      case uhal::defs::INCREMENTAL:
	s += " inc";
	break;
      case uhal::defs::NON_INCREMENTAL:
	s += " non-inc";
	break;
      case uhal::defs::HIERARCHICAL:
      case uhal::defs::SINGLE:
      default:
	break;
      }
      switch( perm) {
      case uhal::defs::READ:
	s += " r";
	break;
      case uhal::defs::WRITE:
	s += " w";
	break;
      case uhal::defs::READWRITE:
	s += " rw";
	break;
      default:
	;
      }
      if( size > 1) {
	char siz[20];
	snprintf( siz, 20, " size=0x%08x", size);
	s += siz;
      }
      printf("  %3d: %-60s (addr=%08x mask=%08x) %s\n", i, nodes[i].c_str(), addr,
	     mask, s.c_str());
      // print long description
      if( describe)
	printf("       %s\n", descr.c_str());

      // print documentation mode with table/row/column
      if( help) {
	  
      }

      // dump params
      if( debug) {
	const boost::unordered_map<std::string,std::string> params = node.getParameters();
	for( boost::unordered_map<std::string,std::string>::const_iterator it = params.begin();
	     it != params.end();
	     it++) {
	  printf( "   %s = %s\n", it->first.c_str(), it->second.c_str());
	}
      }
    }
    
  return 0;
}

  
  



int Launcher::Write(std::vector<std::string> strArg,std::vector<uint64_t> intArg) {

  if (strArg.size() ==0){
    printf("Need at least an address\n");
    return 0;
  }
  std::string saddr = strArg[0];
  //  std::transform( saddr.begin(), saddr.end(), saddr.begin(), ::toupper);

  try {
    uint32_t addr;
    uint32_t mask;

    switch( strArg.size()) {
    case 1:			// address only means masked write
      printf("Mask write to %s\n", saddr.c_str() );
      addr = defaultModule()->hw->getNode(saddr).getAddress();
      mask = defaultModule()->hw->getNode(saddr).getMask();      
      defaultModule()->hw->getClient().write(addr,mask);
      defaultModule()->hw->dispatch();
      break;
    case 2:
      printf("Write to ");
      if( isdigit( saddr.c_str()[0])) {
	printf("address %s\n", saddr.c_str() );
	defaultModule()->hw->getClient().write(uint32_t(intArg[0]),uint32_t(intArg[1]));
      } else {
	printf("register %s\n", saddr.c_str());
	defaultModule()->hw->getNode(saddr).write(uint32_t(intArg[1]));
      }
      break;
    default:
      printf("Expect address and optional value only\n");
      break;
    }	

  } catch( const uhal::exception::NoBranchFoundWithGivenUID ) {
    printf("Register name not found: %s\n", saddr.c_str() );
  }

  return 0;
}
  
